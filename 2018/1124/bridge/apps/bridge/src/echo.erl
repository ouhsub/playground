-module(echo).
-export([init/2, terminate/2]).

init(Req, State) ->
    Resp = case cowboy_req:method(Req) of
        <<"POST">> ->
            {ok, Body, _Req} = cowboy_req:read_body(Req),
            cowboy_req:reply(200, #{<<"content-type">> => <<"text/plain">>}, Body, Req);
        Method ->
            io:format("Method is ~p~n", [Method]),
            cowboy_req:reply(200, #{<<"content-type">> => <<"text/plain">>}, <<"no post">>, Req)
    end,
    {ok, Resp, State}.

terminate(_Reason, _State) ->
    ok.
