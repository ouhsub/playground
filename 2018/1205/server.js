const Koa = require('koa');
const app = new Koa();
const static = require('koa-static');
const fs = require('fs');
const sleep = require('sleep-promise');


app.use(static(__dirname + '/public/'));

app.use(async ctx => {
    await sleep(5000);
    ctx.body = 'Hello Sleep';
});

app.listen(3000);