﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

	public GameObject[] outWallArray;
	public GameObject[] floorArray;
	public GameObject[] wallArray;
	public GameObject[] foodArray;

	public int rows = 10;
	public int cols = 10;

	public int minCountWall = 2;
	public int maxCountWall = 8;

	private Transform mapHolder;
	private List<Vector2> positionList = new List<Vector2> ();

//	private GameManager gameManager;

	// Use this for initialization
	void Start () {
//		gameManager = this.GetComponent<GameManager> ();
		InitMap ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 初始化地图
	public void InitMap () {
		mapHolder = new GameObject ("Map").transform;
		// 创建外墙和地板
		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows; y++) {
				if (x == 0 || y == 0 || x == cols - 1 || y == rows - 1) {
					int index = Random.Range (0, outWallArray.Length);
					GameObject outWallItem = GameObject.Instantiate (outWallArray [index], new Vector3 (x, y, 0), Quaternion.identity);
					outWallItem.transform.SetParent (mapHolder);
				} else {
					int index = Random.Range (0, floorArray.Length);
					GameObject floorItem = GameObject.Instantiate (floorArray [index], new Vector3 (x, y, 0), Quaternion.identity);
					floorItem.transform.SetParent (mapHolder);
				}
			}
		}

		// 创建障碍物，食物和敌人
		positionList.Clear();
		for (int x = 2; x < cols - 2; x++) {
			for (int y = 2; y < rows - 2; y++) {
				positionList.Add (new Vector2 (x, y));
			}
		}

		int wallCount = Random.Range (minCountWall, maxCountWall+1);
		for (int i = 0; i < wallCount; i++) {
			Vector2 pos = RandomPosition ();
			int wallIndex = Random.Range (0, wallArray.Length);
			GameObject wallItem = GameObject.Instantiate (wallArray [wallIndex], pos, Quaternion.identity) as GameObject;
			wallItem.transform.SetParent (mapHolder);
		}

//		int foodCount = Random.Range (2, gameManager.level * 2 + 1);
//		for (int i = 0; i < foodCount; i++) {
//			Vector2 pos = RandomPosition ();
//
//		}
	}

	private Vector2 RandomPosition() {
		int positionIndex = Random.Range (0, positionList.Count);
		Vector2 pos = positionList [positionIndex];
		positionList.RemoveAt (positionIndex);
		return pos;
	}
}
