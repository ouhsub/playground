# binary-chat-room
## skill stack
* Socket.io 
* protobuf.js
* express

## issues
* 指定socketio只能通过websocket进行通信
    - `{transports: ['websocket']}`
* 通过WebSocket传送二进制流而不是字符串
    - `new Blob(buffer)`
* 二进制buffer转换为JS-UInt8Array对象
    -  `new Uint8Array(buffer)`

## TODOs
* JS二进制数组`ArrayBuffer`
* protobuf的协议文件*.proto