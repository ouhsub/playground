var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http, {
    transports: ['websocket']
  });

app.use(express.static('public'));
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

var protobuf = require("protobufjs");

protobuf.load("awesome.proto", function(err, root) {
    if (err)
        throw err;
 
    // Obtain a message type
    var AwesomeMessage = root.lookupType("awesomepackage.AwesomeMessage");
 
    // Exemplary payload
    var payload = { awesomeField: "AwesomeString" };
 
    // Verify the payload if necessary (i.e. when possibly incomplete or invalid)
    var errMsg = AwesomeMessage.verify(payload);
    if (errMsg)
        throw Error(errMsg);
 
    // Create a new message
    var message = AwesomeMessage.create(payload); // or use .fromObject if conversion is necessary
 
    // Encode a message to an Uint8Array (browser) or Buffer (node)
    var buffer = AwesomeMessage.encode(message).finish();
    // ... do something with buffer
 
    // Decode an Uint8Array (browser) or Buffer (node) to a message
    var message = AwesomeMessage.decode(buffer);
    // ... do something with message
 
    // If the application uses length-delimited buffers, there is also encodeDelimited and decodeDelimited.
 
    // Maybe convert the message back to a plain object
    var object = AwesomeMessage.toObject(message, {
        longs: String,
        enums: String,
        bytes: String,
        // see ConversionOptions
    });
    io.on('connection', function(socket) {
        console.log('a user connected');
        io.emit('chat__message', buffer);
        socket.broadcast.emit('hi');
        socket.on('chat_message', function(msg) {
            console.log(msg)
            
            let message = AwesomeMessage.decode(msg);
            console.log(message);
        })
        socket.on('disconnect', function() {
            console.log('user disconnected');
        })
    })
});


http.listen(3000, function(){
    console.log('listening on *:3000');
});