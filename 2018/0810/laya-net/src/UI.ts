export default class UI {
    public textInput: Laya.TextInput;
    public loginBtn: Laya.Button;
    private _status: number = 0;
    private _usersBox: Laya.List;
    private _sp: Laya.Sprite;
    private _userList: Array<any>;
    public username: string;
    public target;

    private _textBox: Laya.Text;

    constructor() {
        this._initTextInput();
        this._initLoginButton();
    }


    private _initTextInput() {
        this.textInput = new Laya.TextInput();
        this.textInput.wordWrap = false;
        this.textInput.prompt  = '请输入用户名';
        this.textInput.fontSize = 30;
        this.textInput.x = Laya.Browser.width/2 - 250;
        this.textInput.y = Laya.Browser.height/2 - 30;
        this.textInput.width = 300;
        this.textInput.height = 60;
        this.textInput.padding = '0, 0, 0, 20';
        this.textInput.borderColor = '#fff';
        this.textInput.color = "#333";
        this.textInput.bgColor = "#fff";
        this.textInput.focus = true;
        Laya.stage.addChild(this.textInput);

        this.textInput.on('focus', this, this._handleFocusedIn);
        this.textInput.select();
    }

    private _initLoginButton() {
        this.loginBtn = new Laya.Button();
        this.loginBtn.label = "Enter";
        this.loginBtn.labelSize = 50;
        this.loginBtn.height = 60;
        this.loginBtn.width = 200;
        this.loginBtn.labelColors= '#fff,#fff,#fff,#fff';
        this.loginBtn.x = Laya.Browser.width/2 + 50;
        this.loginBtn.y = Laya.Browser.height/2 - 30;
        Laya.stage.addChild(this.loginBtn);
    }

    private _initUsersBox() {
        this._usersBox = new Laya.List();
        this._usersBox.width = 600;
        this._usersBox.height = Laya.Browser.height;
        this._usersBox.itemRender = UserItem;
        this._usersBox.x = 0;
        this._usersBox.y = 0;
        this._usersBox.vScrollBarSkin = "";
        this._usersBox.selectEnable = true;
        this._usersBox.selectHandler = new Laya.Handler(this, this._selectUserItem);

        this._usersBox.renderHandler = new Laya.Handler(this, this._updateItem);

        Laya.stage.addChild(this._usersBox);

    }

    private _selectUserItem(index: number): void {
        let user = this._userList[index];
        console.log(user);
        if (user.username === this.username) {
            return;
        } else {
            this.textInput.text = `@${user.username} `;
            this.textInput.focus = true;
            this.textInput.setSelection(-1,-1);
            this.target = user;
            this.changeStatus(2);
        }
    }
    private _updateItem(cell: UserItem, index: number): void {
        cell.setUserName(cell.dataSource);
    }

    private _handleFocusedIn() {
        this.textInput.select();
    }

    public changeStatus(status: number): void {
        this._status = status;
    }

    public getStatus(): number {
        return this._status;
    }

    public clearInput(): void {
        this.textInput.text = '';
    }
    
    public layoutChat(): void {
        Laya.Tween.to(this.textInput, { x : 650 }, 200, Laya.Ease.linearIn, null);
        Laya.Tween.to(this.textInput, { y : Laya.Browser.height - 100 }, 200, Laya.Ease.linearIn, null);
        Laya.Tween.to(this.textInput, { width : Laya.Browser.width - 900 }, 200, Laya.Ease.linearIn, null);
        Laya.Tween.to(this.loginBtn, { x : Laya.Browser.width - 250 }, 200, Laya.Ease.linearIn, null);
        Laya.Tween.to(this.loginBtn, { y : Laya.Browser.height - 100 }, 200, Laya.Ease.linearIn, null);
        this.textInput.prompt  = '请输入消息';
        this.loginBtn.label = "Post";
        this._initUsersBox();
        this._initTextBox();
    }

    private _initTextBox() {

        this._textBox = new Laya.Text();
        this._textBox.overflow = Laya.Text.SCROLL;
        this._textBox.text = "";
        this._textBox.size(Laya.Browser.width - 600,Laya.Browser.height - 100);
        this._textBox.x = 600;
        this._textBox.y = 0;
        this._textBox.padding = [50,50,50,50];
        this._textBox.fontSize = 40;
        this._textBox.color = "#ccc";
        Laya.stage.addChild(this._textBox);
    }

    public showMsgs(msgList: Array<any>): void {
        let msgs = '';
        for (let i=0; i<msgList.length; i++) {
            msgs += msgList[i].msg + '\n';
        }

        this._textBox.text = msgs;
        this._textBox.scrollY = this._textBox.textHeight;
    }

    public showUsers(users: Array<any>): void {
        this._usersBox.array = users;
        this._userList = users;
    }
}

class UserItem extends Laya.Box {
    public static WID: number = 600;
    public static HEI: number = 100;

    private _username: Laya.Text;
    constructor(username){
        super();
        this.size(UserItem.WID, UserItem.HEI);
        this.x = 0;
        this.y = 0;
        this._username = new Laya.Text();
        this._username.size(600, 100);
        this._username.fontSize = 50;
        this._username.color = '#fff';
        this._username.padding = [25,0,0,20];
        this._username.text = username;
        this.addChild(this._username);
    }
    setUserName(user) {
        this._username.text = user.username;
    }
}