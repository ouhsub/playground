import Socket from './Socket';
import UI from './UI';

class Main {
    private _buf: Laya.Byte;
    private _socket: Laya.Socket;
    private _socketUUID: string;
    private _socketMsg;
    private _username: string;
    private _ui: UI;

    public msgList: Array<any> = [];
    public userList: Array<any> = [];

    constructor() {
        Laya.init(Laya.Browser.width, Laya.Browser.height, Laya.WebGL);
        
        this._initProto();
        this._addEventListeners();
        this._initUI();
    }

    private _addEventListeners() {
        window.addEventListener('resize', function() {
            Laya.stage.width = Laya.Browser.width;
            Laya.stage.height = Laya.Browser.height;
        });
    }

    private _initUI() {
        this._ui = new UI();
        this._ui.loginBtn.clickHandler = Laya.Handler.create(this, this._handleEnter);
        this._ui.textInput.on('enter', this, this._handleEnter);
    }

    private _handleEnter() {
        if (this._ui.textInput.text.length == 0) {
            return;
        }
        let status = this._ui.getStatus();
        switch (status) {
            case 0:
                this._ui.username = this._ui.textInput.text;
                this._login();
                break;
            case 2: 
                let singleMsg = {
                    action: 'SinglePost',
                    msg: JSON.stringify({
                        text: this._ui.textInput.text,
                        target: this._ui.target
                    })
                };
                this._sendMSG(singleMsg);
                this._ui.changeStatus(1);
                this.msgList.push({
                    action: 'self',
                    msg: `#..#: ${this._ui.textInput.text}`,
                });
                this._ui.showMsgs(this.msgList);
                break;
            default:
                let msg = {
                    action: 'CommonMsg',
                    msg: this._ui.textInput.text,
                };
                this._sendMSG(msg);
                this.msgList.push({
                    action: 'CommonMsg',
                    msg: `#..#: ${this._ui.textInput.text}`,
                });
                this._ui.showMsgs(this.msgList);
                break;
        }
        this._ui.clearInput();
    }

    private _login() {
        this._username = this._ui.textInput.text;
        this._initSocket();
        this._ui.changeStatus(1);
        this._ui.layoutChat();
    }
    
    private _initSocket() {
        this._socket = new Laya.Socket();
        this._socket.endian = Laya.Socket.LITTLE_ENDIAN;
    
        this._socket.connectByUrl('ws://localhost:3030');

        this._socket.on(Laya.Event.OPEN, this, this._handleSocketOpen);
        this._socket.on(Laya.Event.MESSAGE, this, this._handleSocketMessage);
        this._socket.on(Laya.Event.CLOSE, this, this._handleSocketClose);
        this._socket.on(Laya.Event.ERROR, this, this._handleSocketError);
    }

    private _handleSocketOpen(): void {
        this._setUserInfo();
    }

    private _handleSocketMessage(msg): void {
        let message = this._socketMsg.decode(new Uint8Array(msg));
        switch (message.action) {
            case 'SetUUID':
                this._socketUUID = message.msg;
                break;
            case 'UpdateUserList':
                this.userList = JSON.parse(message.msg);
                this._ui.showUsers(this.userList);
                break;
            default:
                this.msgList.push(message);
                this._ui.showMsgs(this.msgList);
                break;
        }
    }

    private _setUserInfo() {
        this._sendMSG({action: 'SetUserInfo', msg: JSON.stringify({username: this._username})});
    }

    private _handleSocketClose(e): void {

    }

    private _handleSocketError(e): void {

    }

    private _sendMSG(msg) {
        let message = this._socketMsg.create(msg);
        let buf = this._socketMsg.encode(message).finish();
        this._socket.send(buf);
    }

    private _initProto() {
        let Root  = protobuf.Root,
            Type  = protobuf.Type,
            Field = protobuf.Field;

        let SocketMessage = new Type("SocketMessage")
            .add(new Field("action", 1, "string"))
            .add(new Field('msg', 2, 'string'));

        let root = new Root().define("socketpackage").add(SocketMessage);
        this._socketMsg = root.lookupType("socketpackage.SocketMessage");
    }
}

new Main();
