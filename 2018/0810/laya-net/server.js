const uuid = require('uuid/v1');
const WebSocket = require('ws');
const protobuf = require("protobufjs");
const clients = {};
const users = [];

/**
 * 初始化Protobuf
 */
const Root  = protobuf.Root,
      Type  = protobuf.Type,
      Field = protobuf.Field;

const SocketMessage = new Type("SocketMessage")
    .add(new Field("action", 1, "string"))
    .add(new Field('msg', 2, 'string'));

const root = new Root().define("socketpackage").add(SocketMessage);
const SocketMsg = root.lookupType("socketpackage.SocketMessage");

/**
 * 初始化WS服务器
 */
const wss = new WebSocket.Server({
    port: 3030,
});

// 设置广播方法
wss.broadcast = function broadcast(data, ws) {
    for (client of wss.clients) {
        if (client.readyState === WebSocket.OPEN) {
            if(ws && ws.uuid === client.uuid){
                continue;
            }
            client.send(data);
        }
    }
};

// 监听socket连接事件
wss.on('connection', function connection(ws) {
    ws.uuid = uuid();
    ws.send(packMSG({
        action: 'SetUUID',
        msg: ws.uuid,
    }))
    clients[ws.uuid] = ws;
    ws.on('message', function incoming(msg) {
        const message = unpackMSG(msg);
        console.log(message);

        switch (message.action) {
            case 'SetUserInfo':
                const userinfo = JSON.parse(message.msg);
                ws.username = userinfo.username;
                let user = {
                    username: ws.username,
                    uuid: ws.uuid,
                };
                users.push(user);
                wss.broadcast(packMSG({
                    action: 'UpdateUserList', 
                    msg: JSON.stringify(users)
                }));
                wss.broadcast(packMSG({
                    action: 'BroadCastMSG', 
                    msg: `Server: user ${ws.username} connected`
                }));
                break;
            case 'SinglePost':
                postSingleTarget(message.msg, ws);
                break;
            default:
                wss.broadcast(packMSG({action: 'UserMSG', msg: `${ws.username}: ${message.msg}`}), ws);
                break;
        }
    });

    ws.on('close', function(close) {
        try{
            delete clients[ws.uuid];
            removeUser(ws.uuid);
            wss.broadcast(packMSG({
                action: 'UpdateUserList', 
                msg: JSON.stringify(users)
            }));
            wss.broadcast(packMSG({
                action: 'BroadCastMSG', 
                msg: `Server: user ${ws.username} disconnected`
            }));
        }catch(e) {
            console.log(e);
        }
    })
});

function packMSG(msg) {
    const message = SocketMsg.create(msg);

    const buf = SocketMsg.encode(message).finish();
    return buf;
}

function unpackMSG(buf) {
    return SocketMsg.decode(buf);
}

function removeUser(uuid) {
    let index = 0;
    for (let i=0; i<users.length; i++) {
        if(users[i].uuid === uuid){
            index = i;
            break;
        }
    }

    users.splice(index, 1);
}

function postSingleTarget(msg, ws) {
    let msgObj = JSON.parse(msg);
    let postMsg = {
        action: 'SingleMsg',
        msg: `${ws.username}: ${msgObj.text}`,
    };

    clients[msgObj.target.uuid].send(packMSG(postMsg));

}