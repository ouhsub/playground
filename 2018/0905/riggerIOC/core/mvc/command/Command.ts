import {BaseWaitable} from '../../coroutine/BaseWaitable'

export abstract class Command extends BaseWaitable {
    abstract execute(arg?: any): void
}