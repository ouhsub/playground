import {Command} from './Command'
import {ModuleContext} from '../context/ModuleContext'

export class ModuleDoneCommand extends Command{
    private _moduleContext: ModuleContext
    constructor() {
        super();
    }

    setModuleContext(mc: ModuleContext) {
        this._moduleContext = mc
    }

    execute() {
        this._moduleContext && this._moduleContext.done()
    }
}