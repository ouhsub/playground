import {IContext} from './IContext'
import {BaseWaitable} from '../../coroutine/BaseWaitable'
import {ModuleDoneCommand} from '../command/ModuleDoneCommand'
import {ApplicationContext} from './ApplicationContext'
import {InjectionBinder} from '../../binder/injectionBinder';
import {CommandBinder} from '../../binder/commandBinder';
import {MediationBinder} from '../../binder/mediationBinder';

export abstract class ModuleContext extends BaseWaitable implements IContext {
    private _appContext: ApplicationContext
    protected doneCommand: ModuleDoneCommand;
    constructor(appContext: ApplicationContext) {
        super()
        this._appContext = appContext
        this.doneCommand = new ModuleDoneCommand()
        
        this.bindInjections()
        this.bindCommands()
        this.bindMediators()

        this.start()
    }

    dispose() {
        this._appContext = null
    }

    abstract bindInjections(): void
    abstract bindCommands(): void
    abstract bindMediators(): void

    protected abstract onStart(): void

    get injectionBinder(): InjectionBinder {
        return this.getInjectionBinder()
    }

    get commandBinder(): CommandBinder {
        return this.getCommandBinder()
    }

    get mediationBinder(): MediationBinder {
        return this.getMediationBinder()
    }

    getInjectionBinder(): InjectionBinder {
        return this._appContext.getInjectionBinder()
    }

    getCommandBinder(): CommandBinder {
        return this._appContext.getCommandBinder()
    }

    getMediationBinder(): MediationBinder {
        return this._appContext.getMediationBinder()
    }

    protected start(): void {
        this.onStart()
    }
}