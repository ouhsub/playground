import {InjectionBinder} from '../../binder/injectionBinder'
import {CommandBinder} from '../../binder/commandBinder'

export interface IContext{
    getInjectionBinder(): InjectionBinder
    getCommandBinder(): CommandBinder
    bindInjections(): void
    bindCommands(): void
    dispose(): void
}