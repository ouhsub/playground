import {IContext} from "./IContext";
import {waitFor} from '../../coroutine/Coroutine'
import {ModuleContext} from "./ModuleContext";
import {inject, InjectionBindInfo, InjectionBinder} from '../../binder/injectionBinder'
import {CommandBinder, SignalCommandBinder} from '../../binder/commandBinder'
import {MediationBinder} from '../../binder/mediationBinder'

export abstract class ApplicationContext implements IContext {
    protected get injectionBinder(): InjectionBinder {
        return InjectionBinder.instance
    }

    @inject(CommandBinder)
    protected commandBinder: CommandBinder

    @inject(MediationBinder)
    protected mediationBinder: MediationBinder

    private _modules: any[]

    constructor() {
        this.bindCommandBinder()
        this.bindMediationBinder()
        this.bindInjections()
        this.bindCommands()
        this._modules = []
        this.registerModuleContexts()
        this.initializeModuleContexts()
    }

    dispose() {

    }

    getInjectionBinder(): InjectionBinder {
        return this.injectionBinder
    }
    
    getCommandBinder(): CommandBinder {
        return this.commandBinder
    }

    getMediationBinder(): MediationBinder {
        return this.mediationBinder
    }

    protected async initializeModuleContexts() {
        let m: ModuleContext
        for (var i=0; i<this._modules.length; i++) {
            let info: InjectionBindInfo = this.injectionBinder.bind(this._modules[i])
            m = new info.realClass(this)
            info.toValue(m)
            await waitFor(m)
        }
    }

    protected addModuleContext(contextCls: any): ApplicationContext {
        this._modules.push(contextCls)
        return this
    }

    abstract bindInjections(): void
    abstract bindCommands(): void
    abstract registerModuleContexts(): void

    protected bindCommandBinder(): void {
        this.injectionBinder
            .bind(CommandBinder)
            .to(SignalCommandBinder)
            .toSingleton()
    }

    protected bindMediationBinder(): void {
        this.injectionBinder
            .bind(MediationBinder)
            .toSingleton()
    }


}