export abstract class Mediator {
    constructor() {}
    abstract onInit(): void
    abstract onShow(): void
    abstract onHise(): void
    abstract dispose(): void
}