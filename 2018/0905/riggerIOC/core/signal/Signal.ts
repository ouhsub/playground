import {ListenerManager} from '../utils/ListenerManager'

export class Signal<T> {
    private _listenerMgr: ListenerManager

    constructor() {}

    dispose() {
        this._listenerMgr && this._listenerMgr.dispose()
        this._listenerMgr = null
    }

    dispatch(args?: T) {
        if (this._listenerMgr) {
            this._listenerMgr.execute(args)
        }
    }

    on(caller, method: (arg: T, ...args: any[]) => any, ...args: any[]) {
        this._makeSureListenerManager()
        this._listenerMgr.on(caller, method, args, false)
    }

    once(caller, method: (arg: T, ...args: any[]) => any, ...args: any[]) {
        this._makeSureListenerManager()
        this._listenerMgr.on(caller, method, args, true)
    }

    off(caller, method: (arg: T, ...args: any[]) => any) {
        if (this._listenerMgr) {
            this._listenerMgr.off(caller, method)
        }
    }

    private _makeSureListenerManager() {
        if (!this._listenerMgr) {
            this._listenerMgr = new ListenerManager()
        }
    }
}