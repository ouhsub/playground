import {Signal} from '../signal/Signal'
import {Command} from '../mvc/command/Command'
import {InjectionBinder, InjectionBindInfo} from "./injectionBinder";
import {waitFor} from "../coroutine/Coroutine";

/****************** Abstract ***********************/
export abstract class CommandBinder {
    abstract bind(cmd: any): CommandBindInfo
}

export abstract class CommandBindInfo {
    constructor(){}

    abstract to(cmd: any): CommandBindInfo // 将信号绑定到命令，可绑定到多个命令
    abstract toValue(cmd: any): CommandBindInfo // 绑定到值
    abstract once(): CommandBindInfo // 设置为一次性绑定
    abstract inSequence(): CommandBindInfo // 设置为顺序命令
}

export class CommandBindTuple {
    ctr: any
    inst: Command
    constructor(cls: any) {
        this.ctr = cls
        this.inst = null
    }
}

class CommandInfo {
    cls: any
    inst: any
}

/************************ Event **************************/
export class EventCommandBindInfo implements CommandBindInfo {
    message: number | string = null
    bindTuples: CommandBindTuple[]

    constructor(msg: number | string) {
        this.message = msg
        this.bindTuples = []
    }

    to(cls: any): EventCommandBindInfo {
        for (var i=0; i<this.bindTuples.length; i++) {
            if (this.bindTuples[i].ctr === cls) {
                return this
            }
        }
        this.bindTuples.push(new CommandBindTuple(cls))

        return this
    }

    toValue(value: any): CommandBindInfo {
        return this
    }
    
    once(): CommandBindInfo {
        return this
    }

    inSequence(): CommandBindInfo {
        return this
    }

    // 将绑定设置为单例模式
    // 对command而言，其总是单例的
    toSingleton(): InjectionBindInfo {
        throw new Error("Command is always singleton.")
    }
}

// 消息与命令的绑定，一个消息可以同时绑定多个命令，即一个消息可导致多个命令的执行
// 一个命令不能同时绑定到多个消息
export class EventCommandBinder implements CommandBinder {
    constructor() {}
    private _commandsMap

    // 绑定消息
    bind(msg: string | number): EventCommandBindInfo {
        let info: EventCommandBindInfo = this.findBindInfo(msg)
        if (!info) {
            return this._commandsMap[msg] = new EventCommandBindInfo(msg)
        }
        return info
    }

    // 查询绑定消息
    findBindInfo(msg: string | number): EventCommandBindInfo {
        let info: EventCommandBindInfo = this._commandsMap[msg]
        if (info) {
            return info
        }
        return null
    }
}

/********************** Signal ************************************/
export class SignalCommandBindInfo {
    private _bindSignal: Signal<any>
    private _commandsCls: CommandInfo[]
    private _isOnce: boolean
    private _isInSequence: boolean
    
    constructor(signal: Signal<any>) {
        this._commandsCls = []
        this._bindSignal = signal
        this._isOnce = false
        this._isInSequence = false
        signal.on(this, this._onSignal)
    }

    dispose() {
        this._bindSignal.off(this, this._onSignal)
        this._bindSignal.dispose()
        this._bindSignal = null
        this._commandsCls = null
    }

    to(cmdCls: any): SignalCommandBindInfo {
        InjectionBinder.instance.bind(cmdCls)
        this._commandsCls.push({cls: cmdCls, inst: null})

        return this
    }

    toValue(value: any) {
        this._commandsCls.push({cls: null, inst: value})

        return this
    }

    once(): SignalCommandBindInfo {
        this._isOnce = true
        return this
    }

    inSquence(): SignalCommandBindInfo {
        this._isInSequence = true
        return this
    }

    private _onSignal(args?: any) {
        this._executeCommands(args)
    }

    private async _executeCommands(args?: any) {
        for (var i=0; i<this._commandsCls.length; i++) {
            let cmd: Command
            let cmdInfo: CommandInfo = this._commandsCls[i]
            if (cmdInfo.inst) {
                cmd = cmdInfo.inst
            } else {
                cmd = InjectionBinder.instance.bind(cmdInfo.cls).getInstance() as Command
            }

            cmd.execute(args)
            if (this._isInSequence) {
                await waitFor(cmd)
            }
        }
        if (this._isOnce) {
            this.dispose()
        }
    }
}

export class SignalCommandBinder {
    constructor() {}
    bind(cls: any): SignalCommandBindInfo {
        return new SignalCommandBindInfo(InjectionBinder.instance.bind(cls).toSingleton().getInstance<Signal<any>>())
    }
}