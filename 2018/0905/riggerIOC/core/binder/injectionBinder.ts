export class InjectionBindInfo {
    cls: any = null
    private _bindCls: any = null
    private _instance: any = null
    private _isSingleton: boolean = false

    constructor(ctr: Function) {
        this.cls = ctr
    }
    get realClass(): any {
        if (this._bindCls) {
            return this._bindCls
        }
    }

    get hasInstance(): boolean {
        return !!this._instance
    }

    dispose() {
        this.cls = null
        this._bindCls = null
        this._instance = null
    }

    to(ctr: Function): InjectionBindInfo {
        if (ctr === this.cls) {
            throw new Error("can not bind to self.")
        }

        this._bindCls = ctr
        return this
    }

    toValue(value: any): InjectionBindInfo {
        this.toSingleton()
        this._instance = value
        return this
    }

    toSingleton(): InjectionBindInfo {
        this._isSingleton = true
        return this
    }

    getInstance<T>(): T {
        if (this._instance) {
            return this._instance
        }
        let inst:T = new (this.realClass)()

        if (this._isSingleton) {
            this._instance = inst
        }

        return inst
    }
}

export class InjectionBinder {
    private static _instance: InjectionBinder
    private _bindedArray: InjectionBindInfo[]
    private _registerKey: string = '_register_key'
    
    static get instance(): InjectionBinder {
        if (!InjectionBinder._instance) {
            InjectionBinder._instance = new InjectionBinder()
        }
        return InjectionBinder._instance
    }
    
    constructor(){}

    bind(injection: any): InjectionBindInfo {
        let info: InjectionBindInfo = this.findBindInfo(injection)
        if (!info) {
            info = new InjectionBindInfo(injection)
            if (!this._bindedArray) {
                this._bindedArray = []
            }
            this._bindedArray.push(info);
        }

        return info;
    }

    registerInjection(target: any, attrName: string): void {
        if (!target[this._registerKey]) {
            target[this._registerKey] = []
        }
        target[this._registerKey].push(attrName)
    }

    inject(obj: any): void {
        let prototype = obj["__proto__"]
        if (!prototype[this._registerKey] || prototype[this._registerKey].length < 1) {
            return
        }
        for (var i=0; i<prototype[this._registerKey].length; i++) {
            obj[prototype[this._registerKey][i]];// ?????
        }
    }

    unbind(cls: any) {
        this.disposeBindInfo(cls)
    }

    findBindInfo(ctr: Function): InjectionBindInfo {
        if (!ctr) {
            return null
        }
        if (!this._bindedArray) {
            return null
        }

        for (var i=0; i<this._bindedArray.length; i++) {
            let info = this._bindedArray[i]
            if (info.cls === ctr) {
                return info
            }
        }
        return null
    }

    disposeBindInfo(cls: any) {
        if (!cls || !this._bindedArray || this._bindedArray.length < 1) {
            return
        }

        let temp: InjectionBindInfo[] = []

        for (var i=0; i<this._bindedArray.length; i++) {
            let info = this._bindedArray[i]
            if (info.cls !== cls) {
                info.dispose() // ?????
                temp.push(info)
            }
        }

        this._bindedArray = temp
    }
}


export function inject(ctr: any) {
    return function(target: any, attrName: string, descripter?: any) {
        if (descripter) {
            doInjectGetterSetter(ctr, target, attrName, descripter)
        } else {
            doInjectAttr(ctr, target, attrName)
        }
    }
}

const injectionAttrKey = '$$'
export function doInjectGetterSetter(key: any, target: any, attrName: string, descripter: any) {
    let info: InjectionBindInfo = InjectionBinder.instance.bind(key)
    let k: string = injectionAttrKey + attrName

    InjectionBinder.instance.registerInjection(target, attrName)
    descripter.get = function() {
        let v = this[k]
        if (v === null || v === undefined) {
            v = this[k] = info.getInstance()
        }
        return v
    }
    descripter.set = function(v) {
        this[k] = v
    }
}

export function doInjectAttr(key: any, target: any, attrName: string) {
    let info: InjectionBindInfo = InjectionBinder.instance.bind(key)
    let k: string = injectionAttrKey + attrName

    InjectionBinder.instance.registerInjection(target, attrName)
    Object.defineProperty(target, attrName, {
        get: function() {
            let v = this[k]
            if (v === null || v === undefined) {
                v = this[k] = info.getInstance()
            }
            return v
        },
        set: function(v) {
            this[k] = v
        },
        enumerable: true,
        configurable: true
    })
}