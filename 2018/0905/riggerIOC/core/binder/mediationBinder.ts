import {View} from '../mvc/view/View'
import {Mediator} from '../mvc/view/Mediator'
import {InjectionBinder, InjectionBindInfo} from './injectionBinder'

export class MediationBindInfo {
    private _viewConstructor: any
    get viewConstructor(): void {
        return this._viewConstructor
    }

    private _bindMediatorConstructor: any
    get bindMediatorConstructor(): any {
        return this._bindMediatorConstructor
    }

    constructor(cls: any) {
        this._viewConstructor = cls
    }

    to(mediator: any): MediationBindInfo {
        this._bindMediatorConstructor = mediator
        return this
    }
}

export class ViewMediatorTuple {
    view: View
    mediator: Mediator
    constructor(view: View, mediator: Mediator) {
        this.view = view
        this.mediator = mediator
    }

    dispose() {
        this.view = null
        this.mediator = null
    }
}

export class MediationBinder {
    private _infos: MediationBindInfo[]
    private _bindTuples: ViewMediatorTuple[]
    constructor() {}

    bind(cls: any): MediationBindInfo {
        if (!this._infos) {
            this._infos = []
        }
        let info: MediationBindInfo = this.findBindInfo(cls)
        if (!info) {
            info = new MediationBindInfo(cls)
            this._infos.push(info)
        }

        return info
    }

    createAndAttach(viewCls: any, view: View): Mediator {
        let info: MediationBindInfo = this.findBindInfo(viewCls)
        if (!info) {
            return null
        }

        if (!info.bindMediatorConstructor) {
            return null
        }

        let injectionInfo: InjectionBindInfo = InjectionBinder.instance.bind(viewCls)
        if (!injectionInfo.hasInstance) {
            injectionInfo.toValue(view)
        }

        let inst: Mediator = InjectionBinder.instance.bind(info.bindMediatorConstructor).getInstance<Mediator>()
        InjectionBinder.instance.inject(inst)

        injectionInfo.toValue(null)
        this.addBindTuple(view, inst)

        return inst
    }

    detach(view: View, mediator: Mediator) {
        let tuples: ViewMediatorTuple[] = this._bindTuples;
        if (!tuples) {
            return
        }

        let len: number = tuples.length
        if (len < 1) {
            return
        }

        let temp: ViewMediatorTuple[] = []
        for (var i=0; i<len; i++) {
            if (tuples[i].view === view) {
                tuples[i].dispose()
            } else {
                temp.push(tuples[i])
            }
        }

        this._bindTuples = temp
    }

    getAttachedMediatorInstance(view: View): Mediator {
        let tuples: ViewMediatorTuple[] = this._bindTuples
        if (!tuples) {
            return null
        }

        let len: number = tuples.length
        if (len < 1) {
            return null
        }
        for (var i=0; i<len; i++) {
            if (tuples[i].view === view) {
                return tuples[i].mediator
            }
        }

        return null
    }

    findBindInfo(viewCls: any): MediationBindInfo {
        let infos: MediationBindInfo[] = this._infos
        if (!infos || infos.length < 1) {
            return null
        }

        let len: number = infos.length
        for (var i=0; i<len; i++) {
            if (viewCls === infos[i].viewConstructor) {
                return infos[i]
            }
        }

        return null
    }

    private addBindTuple(view: View, mediator: Mediator): void {
        if (!this._bindTuples) {
            this._bindTuples = []
        }

        this._bindTuples.push(new ViewMediatorTuple(view, mediator))
    }
    
}