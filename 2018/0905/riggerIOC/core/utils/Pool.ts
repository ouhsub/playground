export class Pool {
    protected objPool
    constructor() {
        this.objPool = {}
    }

    getPoolBySign(sign: string): any[] {
        let arr: any[] = this.objPool[sign]
        if (!arr) {
            this.objPool[sign] = arr = []
        }

        return arr
    }

    clearBySign(sign: string) {
        delete this.objPool[sign]
    }

    recover(sign: string, item: any) {
        let old: any[] = this.getPoolBySign(sign)
        old.push(item)
    }

    getItemByClass<T>(sign, cls): T {
        let obj:T = this.getItem<T>(sign)
        if (obj) {
            return obj
        }
        return new cls()
    }

    getItemByCreateFun<T>(sign, creator): T {
        let obj: T = this.getItem<T>(sign)
        if (obj) {
            return obj
        }
        return creator();
    }

    getItem<T>(sign): T {
        let pool: T[] = this.getPoolBySign(sign)
        if (pool.length < 1) {
            return null
        }
        return pool.pop()
    }    
}