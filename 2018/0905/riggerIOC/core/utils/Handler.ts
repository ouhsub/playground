import {Pool} from './Pool'
export class Handler {
    static pool: Pool
    private static riggerHandlerSign = '_riggerHandlerSign'
    protected id: number
    private _caller
    private _method
    private _isOnce
    private _args

    get caller() {
        return this._caller
    }

    get method() {
        return this._method
    }

    get isOnce() {
        return this._isOnce
    }

    get args() {
        return this._args
    }

    constructor(caller, method, args, once=false) {
        this._caller = caller
        this._method = method
        this._args = args
        this._isOnce = once

        if (!Handler.pool) {
            Handler.pool = new Pool()
        }
    }

    dispose() {
        this._caller = null
        this._method = null
        this._args = null
    }

    static create(caller, method, args, once=false): Handler {
        let ret = Handler.pool.getItem<Handler>(Handler.riggerHandlerSign)
        if (ret) {
            ret._caller = caller
            ret._method = method
            ret._args = args
            ret._isOnce = once

            return ret
        }

        return new Handler(caller, method, args, once)
    }

    static recover(handler: Handler) {
        handler.dispose()
        Handler.pool.recover(Handler.riggerHandlerSign, handler)
    }

    recover() {
        Handler.recover(this)
    }

    once() {
        this._isOnce = true
    }

    run() {
        if (this._method) {
            let ret = this._method.apply(this._caller, this._args)
            if (this._isOnce) {
                this.dispose()
            }
            return ret
        }
    }

    runWith(args) {
        if (!args) {
            return this.run()
        }

        if (this._method) {
            let ret
            if (args) {
                ret = this._method.apply(this._caller, args.concat(this._args))
            } else {
                ret = this._method.apply(this._caller, this._args)
            }

            if (this._isOnce) {
                this.dispose()
            }
            return ret
        }
        return null
    }
}
