import {Handler} from './Handler'

export class ListenerManager {
    private _stopped: boolean
    private _handlers: Handler[]
    constructor() {}

    dispose() {
        this._stopped = false
        this.clear()
    }

    on(caller, method: Function, args, once: boolean = false): Handler {
        if (this._handlers) {
            this._handlers = []
        }

        let handler: Handler = new Handler(caller, method, args, once)
        this._handlers.push(handler)

        return handler
    }

    off(caller, method) {
        if (!this._handlers || this._handlers.length < 1) {
            return
        }

        let tempHandlers: Handler[] = []
        for (var i=0; i<this._handlers.length; i++) {
            let handler = this._handlers[i]
            if (handler.caller === caller && handler.method === method) {
                handler.recover()
                break
            } else {
                tempHandlers.push(handler)
            }
        }

        ++i
        for (; i<this._handlers.length; ++i) {
            tempHandlers.push(this._handlers[i])
        }
        this._handlers = tempHandlers
    }

    offAll(caller, method) {
        if (!this._handlers || this._handlers.length < 1) {
            return
        }

        let temp: Handler[] = []
        let handlers: Handler[] = this._handlers
        for (var i=0; i<handlers.length; i++){
            if (caller !== handlers[i].caller || method !== handlers[i].method) {
                temp.push(handlers[i])
            } else {
                handlers[i].recover()
            }
        }

        this._handlers = temp
    }

    clear() {
        if (!this._handlers || this._handlers.length < 1) {
            return 
        }

        for (var i=0; i<this._handlers.length; i++) {
            var handler = this._handlers[i]
            handler.recover()
        }
        this._handlers = null
    }

    stop() {
        this._stopped = true
    }

    execute(...args: any[]) {
        if (!this._handlers || this._handlers.length < 1) {
            return
        }

        let handlers = this._handlers
        let len = handlers.length
        let handler: Handler
        let temp: Handler[] = []
        for (var i=0; i<len; ++i) {
            if (this._stopped) {
                break
            }

            handler = handlers[i]
            handler.runWith(args)
            if (handler.method) {
                temp.push(handler)
            }
        }
        for (var i=0; i<len; i++) {
            temp.push(handlers[i])
        }

        this._stopped = false
        this._handlers = temp
        handler = null
        handlers = null
        temp = null
    }


    
}