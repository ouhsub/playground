import { IWaitable } from './IWaitable'

export class BaseWaitable implements IWaitable {
    protected _isDone: boolean = false
    protected _callback: Function
    
    constructor(){}
    
    isDone(): boolean {
        return this._isDone
    }
    
    done(): void {
        this._isDone = true
        this._callback && this._callback()
        this._callback = null
    }
    
    setDoneCallback(cb: Function): void {
        this._callback = cb
    }
    
    reset(): BaseWaitable {
        this._isDone = false
        this._callback = null

        return this
    }
}