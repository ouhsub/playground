export interface IWaitable {
    isDone(): boolean;
    done(): void;
    setDoneCallback(cb: Function): void;
}