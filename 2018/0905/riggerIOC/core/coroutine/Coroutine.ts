import { BaseWaitable } from "./BaseWaitable";

export async function startCoroutine(caller: any, method: Function, ...args: any[]) {
    await method.apply(caller, args)
}

export function waitForNextFrame() {
    return new Promise(resolve => {
        setTimeout(resolve, 0)
    })
}

export function waitForSeconds(ms: number) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

export function waitFor(coroutine: BaseWaitable) {
    return new Promise(resolve => {
        if (coroutine.isDone()) {
            resolve()
        } else {
            coroutine.setDoneCallback(resolve)
        }
    })
}