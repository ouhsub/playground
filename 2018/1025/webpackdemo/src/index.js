// @flow
class Person {
  name: string = 'ouhsub';

  say(): string {
    return `Hello ${this.name}!`;
  }
}

const ouhsub: Person = new Person();
ouhsub.say();


function test(x: number): number {
  return x + 10;
}

const res: number = test(2);
console.log(res);
