module.exports = {
  "extends": "airbnb-base",
  "plugins":[
    "flowtype",
  ],
  "parser": "babel-eslint",
  "rules":{
    "no-console": "off",
  },
  "settings":{
    "flowtype":{
      "onlyFilesWithFlowAnnotation": true,
    }
  }
};
