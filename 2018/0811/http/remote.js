const http = require('http');

http.createServer(function(req, res) {
    console.log('request come: ', req.url);
    
    let data = {
        msg: 'hello World!'
    }

    let resStr = JSON.stringify(data);
    res.writeHead(200, {
        'Access-Control-Allow-Origin': 'http://localhost:3000',
        'Access-Control-Allow-Headers': 'X-CORS-TEST',
        'Access-Control-Allow-Methods': 'PUT, DELETE',
        'Access-Control-Max-Age': '1000'
    })

    res.end(resStr);
}).listen(3030);