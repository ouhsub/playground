#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <png.h>

int main(){
    FILE *fp;
    fp = fopen((char*)"heguan.png", "rb");

    png_byte sig[8];
    fread(sig, 1, 8, fp);
    if(png_sig_cmp(sig, 0, 8)){
            fclose(fp);
            return 0;
        }

    png_structp png_ptr;
    png_infop info_ptr;

    png_ptr = png_create_read_struct(png_get_libpng_ver(NULL), NULL, NULL, NULL);
    info_ptr = png_create_info_struct(png_ptr);
    setjmp(png_jmpbuf(png_ptr));
    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8); 
    png_read_info(png_ptr, info_ptr);

    return 0;
}