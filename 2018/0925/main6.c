#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#define LENGTH 2000

int main() {
    char c[LENGTH];
    int f, i;
    puts("输入要保存的文件信息: ");
    if((i = read(0, c, LENGTH)) < 1){
        perror("读取失败");
        return 1;
    }
    f = open("outfile", O_RDWR | O_CREAT, 0664);

    if(f != -1){
        if(write(f, c, i) != i){
            perror("写入失败");
        }

        puts("保存文件成功");
        close(f);
    }else{
        perror("打开文件");
    }


    return 0;
}