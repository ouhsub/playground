#include <fcntl.h> // open()
#include <sys/types.h> // mode_t
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>


void scan_dir(char *dir, int depth) {
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
    if ((dp = opendir(dir)) == NULL) {
        puts("无法打开目录");
        return;
    }

    chdir(dir);
    while((entry = readdir(dp)) != NULL ){
        lstat(entry->d_name, &statbuf);
        if(S_IFDIR & statbuf.st_mode){
            if(strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0) {
                continue;
            }
            printf("%*s%s/\n", depth, "", entry->d_name);
            scan_dir(entry->d_name, depth+4);
        }else{
            printf("%*s%s\n", depth, "", entry->d_name);
        }
    }
    chdir("..");
    closedir(dp);
}

int main() {
    puts("扫描／目录: ");
    scan_dir("/Users/ouhsub/Documents/langs", 0);
    puts("扫描结束");

    return 0;
}
