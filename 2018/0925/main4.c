#include <fcntl.h> // open()
#include <sys/types.h> // mode_t
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

int main() {
    if(mkdir("testdir", 0774) != -1) {
        puts("目录创建成功");
    }else{
        return 1;
    }

    if(creat("testdir/test1", 0771) != -1) {
        puts("创建文件成功");
    }else{
        return 1;
    }

    sleep(3);

    if(rmdir("testdir") == -1){
        puts("删除目录失败");
    }

    if (unlink("testdir/test1") != -1) {
        puts("删除文件成功");
    } else {
        return 1;
    }

    if (rmdir("testdir") != -1) {
        puts("删除目录成功");
    } else {
        return 1;
    }

    return 0;
}