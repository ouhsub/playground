#include <stdio.h>
#include <ctype.h>

int main() {
    char var;
    printf("Enter your var: ");
    scanf("%c", &var);

    if(isalpha(var)){
        printf("var = |%c| is alphabet\n", var);
    }else{
        printf("var = |%c| is not alphabet\n", var);
    }

    return 0;
}