
const fs = require('fs');
const PNG = require('pngjs').PNG;
const pngFile=process.argv[2]; //获取图片名称
const outDir=pngFile.replace(".png",""); // 设定输出路径
const config = require(`./${outDir}.json`);

fs.createReadStream(pngFile)
    .pipe(new PNG())
    .on('parsed', function() {

        let tempData= new Buffer(4*this.width*this.height);
        this.data.copy(tempData);
        if(!fs.existsSync('export')){
            fs.mkdirSync('export');
        }
        if(!fs.existsSync('export/'+outDir)){
            fs.mkdirSync('export/'+outDir);
        }
        let spritesArray=getSprites(config);
        for(let i=0;i<spritesArray.length;i++){
            let rect=spritesArray[i];
            let newPng = new PNG({
                width: rect.width,
                height: rect.height
            });
            this.bitblt(newPng, rect.x, rect.y, rect.width, rect.height, 0, 0);
            let dst = fs.createWriteStream('export/'+outDir+'/'+rect.name);
            newPng.pack().pipe(dst);
        }
        console.log('Done!');
    }
);
function getSprites(config){
    let spritesArray = [];
    let frames = config.frames;
    for(let key in frames){
        let png = {
            name: key,
            x: frames[key].frame.x,
            y: frames[key].frame.y,
            width: frames[key].frame.w,
            height: frames[key].frame.h
        }
        spritesArray.push(png);
    }
    return spritesArray;
}
